/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_vortex.h
 * Author: frankiezafe
 *
 * Created on January 11, 2019, 6:23 PM
 */

#ifndef FUTARI_VORTEX_H
#define FUTARI_VORTEX_H

#include "core/rid.h"
#include "scene/3d/spatial.h"

// required to avoid division by 0 in futari material shader!
#define FUTARI_VORTEX_MINRANGE 0.001

class FutariVortex;

struct FutariVortexData {
    const FutariVortex* ptr;
    int v_uid;
    String v_uid_s;
    Vector3 position;
    Vector3 orientation;
    real_t strength;
    real_t range;

    bool changed;
    bool changed_position;
    bool changed_orientation;
    bool changed_strength;
    bool changed_range;

    FutariVortexData( ) :
    ptr( 0 ), v_uid( -1 ),
    strength( 0 ),
    range( FUTARI_VORTEX_MINRANGE ),
    changed( false ),
    changed_position( false ),
    changed_orientation( false ),
    changed_strength( false ),
    changed_range( false ) {

    }

    void reset( ) {

        changed = false;
        changed_position = false;
        changed_orientation = false;
        changed_strength = false;
        changed_range = false;

    }

    void operator=( const FutariVortexData& src ) {

        ptr = src.ptr;
        v_uid = src.v_uid;
        v_uid_s = src.v_uid_s;
        position = src.position;
        strength = src.strength;
        range = src.range;
        changed = src.changed;
        changed_position = src.changed_position;
        changed_orientation = src.changed_orientation;
        changed_strength = src.changed_strength;
        changed_range = src.changed_range;

    }

};

class FutariVortex : public Spatial {
    GDCLASS( FutariVortex, Spatial )

public:

    FutariVortex( );
    virtual ~FutariVortex( );

    void set_range( real_t r );
    real_t get_range( ) const;

    void set_barycentre( real_t r );
    real_t get_barycentre( ) const;

    void set_strength( real_t r );
    real_t get_strength( ) const;

    void get_vortex_data( FutariVortexData& fwd ) const;

protected:

    static void _bind_methods( );

    real_t _range;
    real_t _barycentre;
    real_t _strength;

    Vector3 _up;

private:

};

#endif /* FUTARI_VORTEX_H */

