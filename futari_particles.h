/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_particles.h
 * Author: frankiezafe
 *
 * Created on January 6, 2019, 7:26 PM
 */

#ifndef FUTARI_PARTICLES_H
#define FUTARI_PARTICLES_H

#include <iostream>
#include <limits>
#include <vector>

#include "core/rid.h"
#include "core/os/main_loop.h"
#include "scene/3d/visual_instance.h"
#include "scene/resources/material.h"
//#include "scene/3d/particles.h"
#include "futari_wind.h"
#include "futari_attractor.h"
#include "futari_vortex.h"
#include "futari_material.h"

class FutariParticles : public GeometryInstance {
private:
    GDCLASS( FutariParticles, GeometryInstance );

public:

    enum FutariDrawOrder {
        FDRAW_ORDER_INDEX,
        FDRAW_ORDER_LIFETIME,
        FDRAW_ORDER_VIEW_DEPTH,
    };

    enum {
        FMAX_DRAW_PASSES = 4
    };

    FutariParticles( );
    virtual ~FutariParticles( );

    void _node_added( Node *p_node );
    void _node_removed( Node *p_node );
    void _internal_process( );

    AABB get_aabb( ) const;
    PoolVector<Face3> get_faces( uint32_t p_usage_flags ) const;

    void set_emitting( bool p_emitting );
    void set_amount( int p_amount );
    void set_lifetime( float p_lifetime );
    void set_one_shot( bool p_one_shot );
    void set_pre_process_time( float p_time );
    void set_explosiveness_ratio( float p_ratio );
    void set_randomness_ratio( float p_ratio );
    void set_visibility_aabb( const AABB &p_aabb );
    void set_use_local_coordinates( bool p_enable );
    void set_process_material( const Ref<Material> &p_material );
    void set_speed_scale( float p_scale );

    bool is_emitting( ) const;
    int get_amount( ) const;
    float get_lifetime( ) const;
    bool get_one_shot( ) const;
    float get_pre_process_time( ) const;
    float get_explosiveness_ratio( ) const;
    float get_randomness_ratio( ) const;
    AABB get_visibility_aabb( ) const;
    bool get_use_local_coordinates( ) const;
    Ref<Material> get_process_material( ) const;
    float get_speed_scale( ) const;

    void set_fixed_fps( int p_count );
    int get_fixed_fps( ) const;

    void set_fractional_delta( bool p_enable );
    bool get_fractional_delta( ) const;

    void set_draw_order( FutariDrawOrder p_order );
    FutariDrawOrder get_draw_order( ) const;

    void set_draw_passes( int p_count );
    int get_draw_passes( ) const;

    void set_draw_pass_mesh( int p_pass, const Ref<Mesh> &p_mesh );
    Ref<Mesh> get_draw_pass_mesh( int p_pass ) const;

    virtual String get_configuration_warning( ) const;

    void restart( );

    AABB capture_aabb( ) const;

protected:

    static void _bind_methods( );
    void _notification( int p_notification );
    virtual void _validate_property( PropertyInfo &property ) const;

    int find_wind( FutariWind* w ) const;
    int find_attractor( FutariAttractor* a ) const;
    int find_vortex( FutariVortex* v ) const;

    void register_wind( FutariWind* w );
    void unregister_wind( FutariWind* w );

    void register_attractor( FutariAttractor* a );
    void unregister_attractor( FutariAttractor* a );
    
    void register_vortex( FutariVortex* v );
    void unregister_vortex( FutariVortex* v );

    void scan_tree( Node* n );

    Vector<FutariWindData> _winds;
    Vector<FutariAttractorData> _attractors;
    Vector<FutariVortexData> _vortices;

    bool _process_material_refresh_wind;
    bool _process_material_refresh_attractor;
    bool _process_material_refresh_vortex;

private:

    RID particles;

    bool one_shot;
    int amount;
    float lifetime;
    float pre_process_time;
    float explosiveness_ratio;
    float randomness_ratio;
    float speed_scale;
    AABB visibility_aabb;
    bool local_coords;
    int fixed_fps;
    bool fractional_delta;

    Ref<FutariMaterial> process_material;

    FutariDrawOrder draw_order;

    Vector<Ref<Mesh> > draw_passes;

};

VARIANT_ENUM_CAST( FutariParticles::FutariDrawOrder )

#endif /* FUTARI_PARTICLES_H */

