/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_wind.cpp
 * Author: frankiezafe
 * 
 * Created on January 8, 2019, 11:19 PM
 */

#include "futari_wind.h"

FutariWind::FutariWind() : _range(FUTARI_WIND_MINRANGE),_strength(0), _front(0, 0, -1) {

}

FutariWind::~FutariWind() {
}

void FutariWind::set_range(real_t r) {
    if (r < FUTARI_WIND_MINRANGE) {
        _range = FUTARI_WIND_MINRANGE;
        return;
    }
    _range = r;
}

real_t FutariWind::get_range() const {
    return _range;
}

void FutariWind::set_strength(real_t r) {
    _strength = r;
}

real_t FutariWind::get_strength() const {
    return _strength;
}

void FutariWind::get_wind_data( FutariWindData& fwd ) const {

    Vector3 p = get_translation();
    Vector3 f = get_transform().basis.xform( _front ) * _strength;
    if ( !is_visible() ) {
        f *= 0;
    }
    
    fwd.ptr = this;
    
    fwd.reset();
    
    fwd.changed_position = fwd.position != p;
    fwd.changed_force = fwd.force != f;
    fwd.changed_range = fwd.range != _range;
    
    fwd.changed = fwd.changed_position || 
            fwd.changed_force || 
            fwd.changed_range;
    
    if ( fwd.changed ) {
        fwd.position = p;
        fwd.force = f;
        fwd.range = _range;
    }

}

void FutariWind::_bind_methods() {

    //Spatial::_bind_methods();

    ClassDB::bind_method(D_METHOD("set_range", "range"), &FutariWind::set_range);
    ClassDB::bind_method(D_METHOD("get_range"), &FutariWind::get_range);

    ClassDB::bind_method(D_METHOD("set_strength", "strength"), &FutariWind::set_strength);
    ClassDB::bind_method(D_METHOD("get_strength"), &FutariWind::get_strength);

    ADD_GROUP("Wind", "wind_");
    String prop = String::num_real(FUTARI_WIND_MINRANGE) + ",100,0.01,or_greater";
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "range", PROPERTY_HINT_RANGE, prop), "set_range", "get_range");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "strength", PROPERTY_HINT_RANGE, "0,100,0.01,or_greater"), "set_strength", "get_strength");

}
