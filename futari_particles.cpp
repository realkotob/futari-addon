/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

#include "futari_attractor.h"


#include "futari_material.h"


/* 
 * File:   futari_particles.cpp
 * Author: frankiezafe
 * 
 * Created on January 6, 2019, 7:26 PM
 */

#include "futari_particles.h"

FutariParticles::FutariParticles() :
_process_material_refresh_wind(false),
_process_material_refresh_attractor(false),
_process_material_refresh_vortex(false) {

    particles = VS::get_singleton()->particles_create();
    set_base(particles);
    set_emitting(true);
    set_one_shot(false);
    set_amount(8);
    set_lifetime(1);
    set_fixed_fps(0);
    set_fractional_delta(true);
    set_pre_process_time(0);
    set_explosiveness_ratio(0);
    set_randomness_ratio(0);
    set_visibility_aabb(AABB(Vector3(-4, -4, -4), Vector3(8, 8, 8)));
    set_use_local_coordinates(true);
    set_draw_passes(1);
    set_draw_order(FDRAW_ORDER_INDEX);
    set_speed_scale(1);

}

FutariParticles::~FutariParticles() {

    VS::get_singleton()->free(particles);

}

void FutariParticles::_bind_methods() {

    ClassDB::bind_method("_node_added", &FutariParticles::_node_added);
    ClassDB::bind_method("_node_removed", &FutariParticles::_node_removed);
    ClassDB::bind_method("_internal_process", &FutariParticles::_internal_process);

    ClassDB::bind_method(D_METHOD("set_emitting", "emitting"), &FutariParticles::set_emitting);
    ClassDB::bind_method(D_METHOD("set_amount", "amount"), &FutariParticles::set_amount);
    ClassDB::bind_method(D_METHOD("set_lifetime", "secs"), &FutariParticles::set_lifetime);
    ClassDB::bind_method(D_METHOD("set_one_shot", "enable"), &FutariParticles::set_one_shot);
    ClassDB::bind_method(D_METHOD("set_pre_process_time", "secs"), &FutariParticles::set_pre_process_time);
    ClassDB::bind_method(D_METHOD("set_explosiveness_ratio", "ratio"), &FutariParticles::set_explosiveness_ratio);
    ClassDB::bind_method(D_METHOD("set_randomness_ratio", "ratio"), &FutariParticles::set_randomness_ratio);
    ClassDB::bind_method(D_METHOD("set_visibility_aabb", "aabb"), &FutariParticles::set_visibility_aabb);
    ClassDB::bind_method(D_METHOD("set_use_local_coordinates", "enable"), &FutariParticles::set_use_local_coordinates);
    ClassDB::bind_method(D_METHOD("set_fixed_fps", "fps"), &FutariParticles::set_fixed_fps);
    ClassDB::bind_method(D_METHOD("set_fractional_delta", "enable"), &FutariParticles::set_fractional_delta);
    ClassDB::bind_method(D_METHOD("set_process_material", "material"), &FutariParticles::set_process_material);
    ClassDB::bind_method(D_METHOD("set_speed_scale", "scale"), &FutariParticles::set_speed_scale);

    ClassDB::bind_method(D_METHOD("is_emitting"), &FutariParticles::is_emitting);
    ClassDB::bind_method(D_METHOD("get_amount"), &FutariParticles::get_amount);
    ClassDB::bind_method(D_METHOD("get_lifetime"), &FutariParticles::get_lifetime);
    ClassDB::bind_method(D_METHOD("get_one_shot"), &FutariParticles::get_one_shot);
    ClassDB::bind_method(D_METHOD("get_pre_process_time"), &FutariParticles::get_pre_process_time);
    ClassDB::bind_method(D_METHOD("get_explosiveness_ratio"), &FutariParticles::get_explosiveness_ratio);
    ClassDB::bind_method(D_METHOD("get_randomness_ratio"), &FutariParticles::get_randomness_ratio);
    ClassDB::bind_method(D_METHOD("get_visibility_aabb"), &FutariParticles::get_visibility_aabb);
    ClassDB::bind_method(D_METHOD("get_use_local_coordinates"), &FutariParticles::get_use_local_coordinates);
    ClassDB::bind_method(D_METHOD("get_fixed_fps"), &FutariParticles::get_fixed_fps);
    ClassDB::bind_method(D_METHOD("get_fractional_delta"), &FutariParticles::get_fractional_delta);
    ClassDB::bind_method(D_METHOD("get_process_material"), &FutariParticles::get_process_material);
    ClassDB::bind_method(D_METHOD("get_speed_scale"), &FutariParticles::get_speed_scale);

    ClassDB::bind_method(D_METHOD("set_draw_order", "order"), &FutariParticles::set_draw_order);
    ClassDB::bind_method(D_METHOD("get_draw_order"), &FutariParticles::get_draw_order);

    ClassDB::bind_method(D_METHOD("set_draw_passes", "passes"), &FutariParticles::set_draw_passes);
    ClassDB::bind_method(D_METHOD("set_draw_pass_mesh", "pass", "mesh"), &FutariParticles::set_draw_pass_mesh);

    ClassDB::bind_method(D_METHOD("get_draw_passes"), &FutariParticles::get_draw_passes);
    ClassDB::bind_method(D_METHOD("get_draw_pass_mesh", "pass"), &FutariParticles::get_draw_pass_mesh);

    ClassDB::bind_method(D_METHOD("restart"), &FutariParticles::restart);
    ClassDB::bind_method(D_METHOD("capture_aabb"), &FutariParticles::capture_aabb);

    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "emitting"), "set_emitting", "is_emitting");
    ADD_PROPERTY(PropertyInfo(Variant::INT, "amount", PROPERTY_HINT_EXP_RANGE, "1,1000000,1"), "set_amount", "get_amount");

    ADD_GROUP("Time", "");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "lifetime", PROPERTY_HINT_EXP_RANGE, "0.01,600.0,0.01"), "set_lifetime", "get_lifetime");
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "one_shot"), "set_one_shot", "get_one_shot");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "preprocess", PROPERTY_HINT_EXP_RANGE, "0.00,600.0,0.01"), "set_pre_process_time", "get_pre_process_time");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "speed_scale", PROPERTY_HINT_RANGE, "0,64,0.01"), "set_speed_scale", "get_speed_scale");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "explosiveness", PROPERTY_HINT_RANGE, "0,1,0.01"), "set_explosiveness_ratio", "get_explosiveness_ratio");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "randomness", PROPERTY_HINT_RANGE, "0,1,0.01"), "set_randomness_ratio", "get_randomness_ratio");
    ADD_PROPERTY(PropertyInfo(Variant::INT, "fixed_fps", PROPERTY_HINT_RANGE, "0,1000,1"), "set_fixed_fps", "get_fixed_fps");
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "fract_delta"), "set_fractional_delta", "get_fractional_delta");

    ADD_GROUP("Drawing", "");
    ADD_PROPERTY(PropertyInfo(Variant::AABB, "visibility_aabb"), "set_visibility_aabb", "get_visibility_aabb");
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "local_coords"), "set_use_local_coordinates", "get_use_local_coordinates");
    ADD_PROPERTY(PropertyInfo(Variant::INT, "draw_order", PROPERTY_HINT_ENUM, "Index,Lifetime,View Depth"), "set_draw_order", "get_draw_order");

    ADD_GROUP("Process Material", "");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "process_material", PROPERTY_HINT_RESOURCE_TYPE, "ShaderMaterial,FutariMaterial"), "set_process_material", "get_process_material");

    ADD_GROUP("Draw Passes", "draw_");
    ADD_PROPERTY(PropertyInfo(Variant::INT, "draw_passes", PROPERTY_HINT_RANGE, "0," + itos(FMAX_DRAW_PASSES) + ",1"), "set_draw_passes", "get_draw_passes");
    for (int i = 0; i < FMAX_DRAW_PASSES; i++) {
        ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "draw_pass_" + itos(i + 1), PROPERTY_HINT_RESOURCE_TYPE, "Mesh"), "set_draw_pass_mesh", "get_draw_pass_mesh", i);
    }

    BIND_ENUM_CONSTANT(FDRAW_ORDER_INDEX);
    BIND_ENUM_CONSTANT(FDRAW_ORDER_LIFETIME);
    BIND_ENUM_CONSTANT(FDRAW_ORDER_VIEW_DEPTH);

    BIND_CONSTANT(FMAX_DRAW_PASSES);

}

void FutariParticles::_node_added(Node *p_node) {

    if (Object::cast_to<FutariWind>(p_node)) {
        register_wind((FutariWind*) p_node);
    } else if (Object::cast_to<FutariAttractor>(p_node)) {
        register_attractor((FutariAttractor*) p_node);
    } else if (Object::cast_to<FutariVortex>(p_node)) {
        register_vortex((FutariVortex*) p_node);
    }

}

void FutariParticles::_node_removed(Node *p_node) {

    if (Object::cast_to<FutariWind>(p_node)) {
        unregister_wind((FutariWind*) p_node);
    } else if (Object::cast_to<FutariAttractor>(p_node)) {
        unregister_attractor((FutariAttractor*) p_node);
    } else if (Object::cast_to<FutariVortex>(p_node)) {
        unregister_vortex((FutariVortex*) p_node);
    }

}

int FutariParticles::find_wind(FutariWind* w) const {

    for (int max = _winds.size(), i = 0; i < max; ++i) {
        if (_winds[i].ptr == w) {
            return i;
        }
    }
    return -1;

}

int FutariParticles::find_attractor(FutariAttractor* a) const {

    for (int max = _attractors.size(), i = 0; i < max; ++i) {
        if (_attractors[i].ptr == a) {
            return i;
        }
    }
    return -1;

}

int FutariParticles::find_vortex(FutariVortex* v) const {

    for (int max = _vortices.size(), i = 0; i < max; ++i) {
        if (_vortices[i].ptr == v) {
            return i;
        }
    }
    return -1;

}

void FutariParticles::register_wind(FutariWind* w) {

    if (find_wind(w) == -1) {
        FutariWindData fwd;
        w->get_wind_data(fwd);
        _winds.push_back(fwd);
        _process_material_refresh_wind = true;
    }

}

void FutariParticles::unregister_wind(FutariWind* w) {

    int i = find_wind(w);
    if (i != -1) {
        _winds.remove(i);
        _process_material_refresh_wind = true;
    }

}

void FutariParticles::register_attractor(FutariAttractor* a) {

    if (find_attractor(a) == -1) {
        FutariAttractorData fad;
        a->get_attractor_data(fad);
        _attractors.push_back(fad);
        _process_material_refresh_attractor = true;
    }

}

void FutariParticles::unregister_attractor(FutariAttractor* a) {

    int i = find_attractor(a);
    if (i != -1) {
        _attractors.remove(i);
        _process_material_refresh_attractor = true;
    }

}

void FutariParticles::register_vortex(FutariVortex* v) {

    if (find_vortex(v) == -1) {
        FutariVortexData fvd;
        v->get_vortex_data(fvd);
        _vortices.push_back(fvd);
        _process_material_refresh_vortex = true;
    }

}

void FutariParticles::unregister_vortex(FutariVortex* v) {

    int i = find_vortex(v);
    if (i != -1) {
        _vortices.remove(i);
        _process_material_refresh_vortex = true;
    }

}

void FutariParticles::_internal_process() {

    Ref<Material> mm = get_process_material();

    if (!Object::cast_to<FutariMaterial>(mm.ptr())) {
        return;
    }

    Ref<FutariMaterial> procmat = get_process_material();


    bool force_wind_update = false;
    bool force_attractor_update = false;
    bool force_vortex_update = false;

    const int wnum = _winds.size();
    if (_process_material_refresh_wind) {

        _process_material_refresh_wind = false;
        // computing new wind ids ( parameters in the shader )
        int w_uid = 0;
        for (int i = 0; i < wnum; ++i, ++w_uid) {
            _winds.write[i].w_uid = w_uid;
            _winds.write[i].w_uid_s = String::num(w_uid);
        }
        // modifying process material to prepare interaction with wind
        procmat->set_wind_count(wnum);
        force_wind_update = true;

    }

    const int anum = _attractors.size();
    if (_process_material_refresh_attractor) {

        _process_material_refresh_attractor = false;
        // computing new attractor ids ( parameters in the shader )
        int a_uid = 0;
        for (int i = 0; i < anum; ++i, ++a_uid) {
            _attractors.write[i].a_uid = a_uid;
            _attractors.write[i].a_uid_s = String::num(a_uid);
        }
        // modifying process material to prepare interaction with wind
        procmat->set_attractor_count(anum);
        force_attractor_update = true;

    }

    const int vnum = _vortices.size();
    if (_process_material_refresh_vortex) {

        _process_material_refresh_vortex = false;
        // computing new vortex ids ( parameters in the shader )
        int v_uid = 0;
        for (int i = 0; i < vnum; ++i, ++v_uid) {
            _vortices.write[i].v_uid = v_uid;
            _vortices.write[i].v_uid_s = String::num(v_uid);
        }
        // modifying process material to prepare interaction with wind
        procmat->set_vortex_count(vnum);
        force_vortex_update = true;

    }

    for (int i = 0; i < wnum; ++i) {

        FutariWindData& fwd = _winds.write[i];
        fwd.ptr->get_wind_data(fwd);

        if (force_wind_update) {

            procmat->set_futari_param_v3("wind_position_" + fwd.w_uid_s, fwd.position);
            procmat->set_futari_param_v3("wind_force_" + fwd.w_uid_s, fwd.force);
            procmat->set_futari_param_real("wind_range_" + fwd.w_uid_s, fwd.range * fwd.range);

        } else if (fwd.changed) {

            if (fwd.changed_position) {
                procmat->set_futari_param_v3("wind_position_" + fwd.w_uid_s, fwd.position);
            }
            if (fwd.changed_force) {
                procmat->set_futari_param_v3("wind_force_" + fwd.w_uid_s, fwd.force);
            }
            if (fwd.changed_range) {
                procmat->set_futari_param_real("wind_range_" + fwd.w_uid_s, fwd.range * fwd.range);
            }

        }

    }

    for (int i = 0; i < anum; ++i) {

        FutariAttractorData& fad = _attractors.write[i];
        fad.ptr->get_attractor_data(fad);

        if (force_attractor_update) {

            procmat->set_futari_param_v3("attractor_position_" + fad.a_uid_s, fad.position);
            procmat->set_futari_param_real("attractor_strength_" + fad.a_uid_s, fad.strength);
            procmat->set_futari_param_real("attractor_range_" + fad.a_uid_s, fad.range * fad.range);

        } else if (fad.changed) {

            if (fad.changed_position) {
                procmat->set_futari_param_v3("attractor_position_" + fad.a_uid_s, fad.position);
            }
            if (fad.changed_strength) {
                procmat->set_futari_param_real("attractor_strength_" + fad.a_uid_s, fad.strength);
            }
            if (fad.changed_range) {
                procmat->set_futari_param_real("attractor_range_" + fad.a_uid_s, fad.range * fad.range);
            }

        }

    }
    
    for (int i = 0; i < vnum; ++i) {

        FutariVortexData& fvd = _vortices.write[i];
        fvd.ptr->get_vortex_data(fvd);

        if (force_vortex_update) {

            procmat->set_futari_param_v3("vortex_position_" + fvd.v_uid_s, fvd.position);
            procmat->set_futari_param_v3("vortex_orientation_" + fvd.v_uid_s, fvd.orientation);
            procmat->set_futari_param_real("vortex_strength_" + fvd.v_uid_s, fvd.strength);
            procmat->set_futari_param_real("vortex_range_" + fvd.v_uid_s, fvd.range * fvd.range);

        } else if (fvd.changed) {

            if (fvd.changed_position) {
                procmat->set_futari_param_v3("vortex_position_" + fvd.v_uid_s, fvd.position);
            }
            if (fvd.changed_orientation) {
                procmat->set_futari_param_v3("vortex_orientation_" + fvd.v_uid_s, fvd.orientation);
            }
            if (fvd.changed_strength) {
                procmat->set_futari_param_real("vortex_strength_" + fvd.v_uid_s, fvd.strength);
            }
            if (fvd.changed_range) {
                procmat->set_futari_param_real("vortex_range_" + fvd.v_uid_s, fvd.range * fvd.range);
            }

        }

    }

}

void FutariParticles::scan_tree(Node* n) {

    _node_added(n);

    int num = n->get_child_count();
    for (int i = 0; i < num; ++i) {
        scan_tree(n->get_child(i));
    }

}

void FutariParticles::_notification(int p_notification) {

    // Node::
    //NOTIFICATION_ENTER_TREE = 10,
    //NOTIFICATION_EXIT_TREE = 11,
    //NOTIFICATION_MOVED_IN_PARENT = 12,
    //NOTIFICATION_READY = 13,
    //NOTIFICATION_PAUSED = 14,
    //NOTIFICATION_UNPAUSED = 15,
    //NOTIFICATION_PHYSICS_PROCESS = 16,
    //NOTIFICATION_PROCESS = 17,
    //NOTIFICATION_PARENTED = 18,
    //NOTIFICATION_UNPARENTED = 19,
    //NOTIFICATION_INSTANCED = 20,
    //NOTIFICATION_DRAG_BEGIN = 21,
    //NOTIFICATION_DRAG_END = 22,
    //NOTIFICATION_PATH_CHANGED = 23,
    //NOTIFICATION_TRANSLATION_CHANGED = 24,
    //NOTIFICATION_INTERNAL_PROCESS = 25,
    //NOTIFICATION_INTERNAL_PHYSICS_PROCESS = 26,
    //NOTIFICATION_POST_ENTER_TREE = 27,

    switch (p_notification) {

        case NOTIFICATION_PAUSED:
        case NOTIFICATION_UNPAUSED:
            if (can_process()) {
                VS::get_singleton()->particles_set_speed_scale(particles, speed_scale);
            } else {

                VS::get_singleton()->particles_set_speed_scale(particles, 0);
            }
            break;

        case Node::NOTIFICATION_ENTER_TREE:
            get_tree()->connect("node_added", this, "_node_added");
            get_tree()->connect("node_removed", this, "_node_removed");
            get_tree()->connect("idle_frame", this, "_internal_process");
            scan_tree(get_tree()->get_edited_scene_root());
            break;

        case Node::NOTIFICATION_EXIT_TREE:
            get_tree()->disconnect("node_added", this, "_node_added");
            get_tree()->disconnect("node_removed", this, "_node_removed");
            get_tree()->disconnect("idle_frame", this, "_internal_process");
            break;

        case MainLoop::NOTIFICATION_WM_QUIT_REQUEST:
            break;
        case MainLoop::NOTIFICATION_WM_GO_BACK_REQUEST:
            break;
        case MainLoop::NOTIFICATION_OS_MEMORY_WARNING:
        case MainLoop::NOTIFICATION_OS_IME_UPDATE:
        case MainLoop::NOTIFICATION_WM_MOUSE_ENTER:
        case MainLoop::NOTIFICATION_WM_MOUSE_EXIT:
        case MainLoop::NOTIFICATION_WM_FOCUS_IN:
        case MainLoop::NOTIFICATION_WM_FOCUS_OUT:
            break;
        case MainLoop::NOTIFICATION_TRANSLATION_CHANGED:
            break;
        case MainLoop::NOTIFICATION_WM_UNFOCUS_REQUEST:
            break;
        case MainLoop::NOTIFICATION_WM_ABOUT:
            break;
        case MainLoop::NOTIFICATION_CRASH:
            break;
        default:
            break;

    };
};

AABB FutariParticles::get_aabb() const {

    return AABB();
}

PoolVector<Face3> FutariParticles::get_faces(uint32_t p_usage_flags) const {

    return PoolVector<Face3>();
}

void FutariParticles::set_emitting(bool p_emitting) {

    VS::get_singleton()->particles_set_emitting(particles, p_emitting);
}

void FutariParticles::set_amount(int p_amount) {

    ERR_FAIL_COND(p_amount < 1);
    amount = p_amount;
    VS::get_singleton()->particles_set_amount(particles, amount);
}

void FutariParticles::set_lifetime(float p_lifetime) {

    ERR_FAIL_COND(p_lifetime <= 0);
    lifetime = p_lifetime;
    VS::get_singleton()->particles_set_lifetime(particles, lifetime);
}

void FutariParticles::set_one_shot(bool p_one_shot) {

    one_shot = p_one_shot;
    VS::get_singleton()->particles_set_one_shot(particles, one_shot);
    if (!one_shot && is_emitting())
        VisualServer::get_singleton()->particles_restart(particles);
}

void FutariParticles::set_pre_process_time(float p_time) {

    pre_process_time = p_time;
    VS::get_singleton()->particles_set_pre_process_time(particles, pre_process_time);
}

void FutariParticles::set_explosiveness_ratio(float p_ratio) {

    explosiveness_ratio = p_ratio;
    VS::get_singleton()->particles_set_explosiveness_ratio(particles, explosiveness_ratio);
}

void FutariParticles::set_randomness_ratio(float p_ratio) {

    randomness_ratio = p_ratio;
    VS::get_singleton()->particles_set_randomness_ratio(particles, randomness_ratio);
}

void FutariParticles::set_visibility_aabb(const AABB &p_aabb) {

    visibility_aabb = p_aabb;
    VS::get_singleton()->particles_set_custom_aabb(particles, visibility_aabb);
    update_gizmo();
    _change_notify("visibility_aabb");
}

void FutariParticles::set_use_local_coordinates(bool p_enable) {

    local_coords = p_enable;
    VS::get_singleton()->particles_set_use_local_coordinates(particles, local_coords);
}

void FutariParticles::set_process_material(const Ref<Material> &p_material) {

    process_material = p_material;
    RID material_rid;
    if (process_material.is_valid())
        material_rid = process_material->get_rid();
    VS::get_singleton()->particles_set_process_material(particles, material_rid);

    update_configuration_warning();
}

void FutariParticles::set_speed_scale(float p_scale) {

    speed_scale = p_scale;
    VS::get_singleton()->particles_set_speed_scale(particles, p_scale);
}

bool FutariParticles::is_emitting() const {

    return VS::get_singleton()->particles_get_emitting(particles);
}

int FutariParticles::get_amount() const {

    return amount;
}

float FutariParticles::get_lifetime() const {

    return lifetime;
}

bool FutariParticles::get_one_shot() const {

    return one_shot;
}

float FutariParticles::get_pre_process_time() const {

    return pre_process_time;
}

float FutariParticles::get_explosiveness_ratio() const {

    return explosiveness_ratio;
}

float FutariParticles::get_randomness_ratio() const {

    return randomness_ratio;
}

AABB FutariParticles::get_visibility_aabb() const {

    return visibility_aabb;
}

bool FutariParticles::get_use_local_coordinates() const {

    return local_coords;
}

Ref<Material> FutariParticles::get_process_material() const {

    return process_material;
}

float FutariParticles::get_speed_scale() const {

    return speed_scale;
}

void FutariParticles::set_draw_order(FutariDrawOrder p_order) {

    draw_order = p_order;
    VS::get_singleton()->particles_set_draw_order(particles, VS::ParticlesDrawOrder(p_order));
}

FutariParticles::FutariDrawOrder FutariParticles::get_draw_order() const {

    return draw_order;
}

void FutariParticles::set_draw_passes(int p_count) {

    ERR_FAIL_COND(p_count < 1);
    draw_passes.resize(p_count);
    VS::get_singleton()->particles_set_draw_passes(particles, p_count);
    _change_notify();
}

int FutariParticles::get_draw_passes() const {

    return draw_passes.size();
}

void FutariParticles::set_draw_pass_mesh(int p_pass, const Ref<Mesh> &p_mesh) {

    ERR_FAIL_INDEX(p_pass, draw_passes.size());

    draw_passes.write[p_pass] = p_mesh;

    RID mesh_rid;
    if (p_mesh.is_valid())
        mesh_rid = p_mesh->get_rid();

    VS::get_singleton()->particles_set_draw_pass_mesh(particles, p_pass, mesh_rid);

    update_configuration_warning();
}

Ref<Mesh> FutariParticles::get_draw_pass_mesh(int p_pass) const {

    ERR_FAIL_INDEX_V(p_pass, draw_passes.size(), Ref<Mesh>());

    return draw_passes[p_pass];
}

void FutariParticles::set_fixed_fps(int p_count) {
    fixed_fps = p_count;
    VS::get_singleton()->particles_set_fixed_fps(particles, p_count);
}

int FutariParticles::get_fixed_fps() const {
    return fixed_fps;
}

void FutariParticles::set_fractional_delta(bool p_enable) {
    fractional_delta = p_enable;
    VS::get_singleton()->particles_set_fractional_delta(particles, p_enable);
}

bool FutariParticles::get_fractional_delta() const {
    return fractional_delta;
}

String FutariParticles::get_configuration_warning() const {

    String warnings;

    bool meshes_found = false;
    bool anim_material_found = false;

    for (int i = 0; i < draw_passes.size(); i++) {
        if (draw_passes[i].is_valid()) {
            meshes_found = true;
            for (int j = 0; j < draw_passes[i]->get_surface_count(); j++) {
                anim_material_found = Object::cast_to<ShaderMaterial>(draw_passes[i]->surface_get_material(j).ptr()) != NULL;
                SpatialMaterial *spat = Object::cast_to<SpatialMaterial>(draw_passes[i]->surface_get_material(j).ptr());
                anim_material_found = anim_material_found || (spat && spat->get_billboard_mode() == SpatialMaterial::BILLBOARD_PARTICLES);
            }
            if (meshes_found && anim_material_found) break;
        }
    }

    anim_material_found = anim_material_found || Object::cast_to<ShaderMaterial>(get_material_override().ptr()) != NULL;
    SpatialMaterial *spat = Object::cast_to<SpatialMaterial>(get_material_override().ptr());
    anim_material_found = anim_material_found || (spat && spat->get_billboard_mode() == SpatialMaterial::BILLBOARD_PARTICLES);

    if (!meshes_found) {
        if (warnings != String())
            warnings += "\n";
        warnings += "- " + TTR("Nothing is visible because meshes have not been assigned to draw passes.");
    }

    if (process_material.is_null()) {
        if (warnings != String())
            warnings += "\n";
        warnings += "- " + TTR("A material to process the particles is not assigned, so no behavior is imprinted.");
    } else {
        const FutariMaterial *process = Object::cast_to<FutariMaterial>(process_material.ptr());
        if (!anim_material_found && process &&
                (process->get_param(FutariMaterial::FUPA_ANIM_SPEED) != 0.0 ||
                process->get_param(FutariMaterial::FUPA_ANIM_OFFSET) != 0.0 ||
                process->get_param_texture(FutariMaterial::FUPA_ANIM_SPEED).is_valid() ||
                process->get_param_texture(FutariMaterial::FUPA_ANIM_OFFSET).is_valid())) {
            if (warnings != String())
                warnings += "\n";
            warnings += "- " + TTR("Particles animation requires the usage of a SpatialMaterial with \"Billboard Particles\" enabled.");
        }
    }

    return warnings;
}

void FutariParticles::restart() {

    VisualServer::get_singleton()->particles_restart(particles);
}

AABB FutariParticles::capture_aabb() const {

    return VS::get_singleton()->particles_get_current_aabb(particles);
}

void FutariParticles::_validate_property(PropertyInfo &property) const {

    if (property.name.begins_with("draw_pass_")) {
        int index = property.name.get_slicec('_', 2).to_int() - 1;
        if (index >= draw_passes.size()) {
            property.usage = 0;
            return;
        }
    }
}