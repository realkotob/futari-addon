/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_attractor.h
 * Author: frankiezafe
 *
 * Created on January 10, 2019, 4:20 PM
 */

#ifndef FUTARI_ATTRACTOR_H
#define FUTARI_ATTRACTOR_H

#include <iostream>

#include "core/rid.h"
#include "scene/3d/spatial.h"

// required to avoid division by 0 in futari material shader!
#define FUTARI_ATTRACTOR_MINRANGE 0.001

class FutariAttractor;

struct FutariAttractorData {
    const FutariAttractor* ptr;
    int a_uid;
    String a_uid_s;
    Vector3 position;
    real_t strength;
    real_t range;

    bool changed;
    bool changed_position;
    bool changed_strength;
    bool changed_range;

    FutariAttractorData( ) :
    ptr( 0 ), a_uid( -1 ),
    strength( 0 ),
    range( FUTARI_ATTRACTOR_MINRANGE ),
    changed( false ),
    changed_position( false ),
    changed_strength( false ),
    changed_range( false ) {

    }

    void reset( ) {

        changed = false;
        changed_position = false;
        changed_strength = false;
        changed_range = false;

    }

    void operator=( const FutariAttractorData& src ) {

        ptr = src.ptr;
        a_uid = src.a_uid;
        a_uid_s = src.a_uid_s;
        position = src.position;
        strength = src.strength;
        range = src.range;
        changed = src.changed;
        changed_position = src.changed_position;
        changed_strength = src.changed_strength;
        changed_range = src.changed_range;

    }

};

class FutariAttractor : public Spatial {
    GDCLASS( FutariAttractor, Spatial )

public:

    FutariAttractor( );
    virtual ~FutariAttractor( );
    
    void set_range( real_t r );
    real_t get_range( ) const;

    void set_strength( real_t r );
    real_t get_strength( ) const;
    
    void get_attractor_data( FutariAttractorData& fwd ) const;

protected:

    static void _bind_methods( );

    real_t _strength;
    real_t _range;

private:

};

#endif /* FUTARI_ATTRACTOR_H */

