/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_attractor.cpp
 * Author: frankiezafe
 * 
 * Created on January 10, 2019, 4:20 PM
 */

#include "futari_attractor.h"

FutariAttractor::FutariAttractor() : _strength(0), _range( FUTARI_ATTRACTOR_MINRANGE ) {
}

FutariAttractor::~FutariAttractor() {
}

void FutariAttractor::set_range(real_t r) {
    if (r < FUTARI_ATTRACTOR_MINRANGE) {
        _range = FUTARI_ATTRACTOR_MINRANGE;
        return;
    }
    _range = r;
}

real_t FutariAttractor::get_range() const {
    return _range;
}

void FutariAttractor::set_strength(real_t r) {
    _strength = r;
}

real_t FutariAttractor::get_strength() const {
    return _strength;
}

void FutariAttractor::get_attractor_data( FutariAttractorData& fwd ) const {

    Vector3 p = get_translation();
    float s = _strength;
    if ( !is_visible() ) {
        s *= 0;
    }
    
    fwd.ptr = this;
    
    fwd.reset();
    
    fwd.changed_position = fwd.position != p;
    fwd.changed_strength = fwd.strength != s;
    fwd.changed_range = fwd.range != _range;
    
    fwd.changed = fwd.changed_position || 
            fwd.changed_strength || 
            fwd.changed_range;
    
    if ( fwd.changed ) {
        fwd.position = p;
        fwd.strength = s;
        fwd.range = _range;
    }

}

void FutariAttractor::_bind_methods() {

    ClassDB::bind_method(D_METHOD("set_range", "range"), &FutariAttractor::set_range);
    ClassDB::bind_method(D_METHOD("get_range"), &FutariAttractor::get_range);

    ClassDB::bind_method(D_METHOD("set_strength", "strength"), &FutariAttractor::set_strength);
    ClassDB::bind_method(D_METHOD("get_strength"), &FutariAttractor::get_strength);

    ADD_GROUP("Attractor", "attractor_");
    String prop = String::num_real(FUTARI_ATTRACTOR_MINRANGE) + ",100,0.01,or_greater";
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "range", PROPERTY_HINT_RANGE, prop), "set_range", "get_range");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "strength", PROPERTY_HINT_RANGE, "-500,500"), "set_strength", "get_strength");

}

