/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_wind.h
 * Author: frankiezafe
 *
 * Created on January 8, 2019, 11:19 PM
 */

#ifndef FUTARI_WIND_H
#define FUTARI_WIND_H

#include <iostream>

#include "core/rid.h"
#include "scene/3d/spatial.h"

// required to avoid division by 0 in futari material shader!
#define FUTARI_WIND_MINRANGE 0.001

class FutariWind;

struct FutariWindData {
    const FutariWind* ptr;
    int w_uid;
    String w_uid_s;
    Vector3 position;
    Vector3 force;
    real_t range;

    bool changed;
    bool changed_position;
    bool changed_force;
    bool changed_range;

    FutariWindData( ) :
    ptr( 0 ), w_uid( -1 ),
    range( FUTARI_WIND_MINRANGE ),
    changed( false ),
    changed_position( false ),
    changed_force( false ),
    changed_range( false ) {

    }

    void reset( ) {

        changed = false;
        changed_position = false;
        changed_force = false;
        changed_range = false;

    }

    void operator=( const FutariWindData& src ) {

        ptr = src.ptr;
        w_uid = src.w_uid;
        w_uid_s = src.w_uid_s;
        position = src.position;
        force = src.force;
        range = src.range;
        changed = src.changed;
        changed_position = src.changed_position;
        changed_force = src.changed_force;
        changed_range = src.changed_range;

    }

};

class FutariWind : public Spatial {
    GDCLASS( FutariWind, Spatial )

public:

    FutariWind( );
    virtual ~FutariWind( );

    void set_range( real_t r );
    real_t get_range( ) const;

    void set_strength( real_t r );
    real_t get_strength( ) const;

    void get_wind_data( FutariWindData& fwd ) const;

protected:
    static void _bind_methods( );

private:

    real_t _range;
    real_t _strength;

    Vector3 _front;

};

#endif /* FUTARI_WIND_H */

